/*
 * Looker API 4.0 Reference
 *
 *  API 4.0 is the current release of the Looker API. API 3.1 is deprecated.  ### Authorization  The classic method of API authorization uses Looker **API** credentials for authorization and access control. Looker admins can create API credentials on Looker's **Admin/Users** page.  API 4.0 adds additional ways to authenticate API requests, including OAuth and CORS requests.  For details, see [Looker API Authorization](https://cloud.google.com/looker/docs/r/api/authorization).   ### API Explorer  The API Explorer is a Looker-provided utility with many new and unique features for learning and using the Looker API and SDKs.  For details, see the [API Explorer documentation](https://cloud.google.com/looker/docs/r/api/explorer).   ### Looker Language SDKs  The Looker API is a RESTful system that should be usable by any programming language capable of making HTTPS requests. SDKs for a variety of programming languages are also provided to streamline using the API. Looker has an OpenSource [sdk-codegen project](https://github.com/looker-open-source/sdk-codegen) that provides several language SDKs. Language SDKs generated by `sdk-codegen` have an Authentication manager that can automatically authenticate API requests when needed.  For details on available Looker SDKs, see [Looker API Client SDKs](https://cloud.google.com/looker/docs/r/api/client_sdks).   ### API Versioning  Future releases of Looker expand the latest API version release-by-release to securely expose more and more of the core power of the Looker platform to API client applications. API endpoints marked as \"beta\" may receive breaking changes without warning (but we will try to avoid doing that). Stable (non-beta) API endpoints should not receive breaking changes in future releases.  For details, see [Looker API Versioning](https://cloud.google.com/looker/docs/r/api/versioning).   ### In This Release  API 4.0 version was introduced to make adjustments to API functions, parameters, and response types to fix bugs and inconsistencies. These changes fall outside the bounds of non-breaking additive changes we can make to the previous API 3.1.  One benefit of these type adjustments in API 4.0 is dramatically better support for strongly typed languages like TypeScript, Kotlin, Swift, Go, C#, and more.  See the [API 4.0 GA announcement](https://developers.looker.com/api/advanced-usage/version-4-ga) for more information about API 4.0.  The API Explorer can be used to [interactively compare](https://cloud.google.com/looker/docs/r/api/explorer#comparing_api_versions) the differences between API 3.1 and 4.0.   ### API and SDK Support Policies  Looker API versions and language SDKs have varying support levels. Please read the API and SDK [support policies](https://cloud.google.com/looker/docs/r/api/support-policy) for more information.
 *
 * The version of the OpenAPI document: 4.0.23.20
 *
 * Generated by: https://openapi-generator.tech
 */

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct User {
    /// Operations the current user is able to perform on this object
    #[serde(rename = "can", skip_serializing_if = "Option::is_none")]
    pub can: Option<::std::collections::HashMap<String, bool>>,
    /// URL for the avatar image (may be generic)
    #[serde(
        rename = "avatar_url",
        default,
        with = "::serde_with::rust::double_option",
        skip_serializing_if = "Option::is_none"
    )]
    pub avatar_url: Option<Option<String>>,
    /// URL for the avatar image (may be generic), does not specify size
    #[serde(
        rename = "avatar_url_without_sizing",
        default,
        with = "::serde_with::rust::double_option",
        skip_serializing_if = "Option::is_none"
    )]
    pub avatar_url_without_sizing: Option<Option<String>>,
    /// API credentials
    #[serde(
        rename = "credentials_api3",
        default,
        with = "::serde_with::rust::double_option",
        skip_serializing_if = "Option::is_none"
    )]
    pub credentials_api3: Option<Option<Vec<crate::models::CredentialsApi3>>>,
    #[serde(rename = "credentials_email", skip_serializing_if = "Option::is_none")]
    pub credentials_email: Option<Box<crate::models::CredentialsEmail>>,
    /// Embed credentials
    #[serde(
        rename = "credentials_embed",
        default,
        with = "::serde_with::rust::double_option",
        skip_serializing_if = "Option::is_none"
    )]
    pub credentials_embed: Option<Option<Vec<crate::models::CredentialsEmbed>>>,
    #[serde(rename = "credentials_google", skip_serializing_if = "Option::is_none")]
    pub credentials_google: Option<Box<crate::models::CredentialsGoogle>>,
    #[serde(rename = "credentials_ldap", skip_serializing_if = "Option::is_none")]
    pub credentials_ldap: Option<Box<crate::models::CredentialsLdap>>,
    #[serde(
        rename = "credentials_looker_openid",
        skip_serializing_if = "Option::is_none"
    )]
    pub credentials_looker_openid: Option<Box<crate::models::CredentialsLookerOpenid>>,
    #[serde(rename = "credentials_oidc", skip_serializing_if = "Option::is_none")]
    pub credentials_oidc: Option<Box<crate::models::CredentialsOidc>>,
    #[serde(rename = "credentials_saml", skip_serializing_if = "Option::is_none")]
    pub credentials_saml: Option<Box<crate::models::CredentialsSaml>>,
    #[serde(rename = "credentials_totp", skip_serializing_if = "Option::is_none")]
    pub credentials_totp: Option<Box<crate::models::CredentialsTotp>>,
    /// Full name for display (available only if both first_name and last_name are set)
    #[serde(
        rename = "display_name",
        default,
        with = "::serde_with::rust::double_option",
        skip_serializing_if = "Option::is_none"
    )]
    pub display_name: Option<Option<String>>,
    /// EMail address
    #[serde(
        rename = "email",
        default,
        with = "::serde_with::rust::double_option",
        skip_serializing_if = "Option::is_none"
    )]
    pub email: Option<Option<String>>,
    /// (DEPRECATED) (Embed only) ID of user's group space based on the external_group_id optionally specified during embed user login
    #[serde(
        rename = "embed_group_space_id",
        default,
        with = "::serde_with::rust::double_option",
        skip_serializing_if = "Option::is_none"
    )]
    pub embed_group_space_id: Option<Option<String>>,
    /// First name
    #[serde(
        rename = "first_name",
        default,
        with = "::serde_with::rust::double_option",
        skip_serializing_if = "Option::is_none"
    )]
    pub first_name: Option<Option<String>>,
    /// Array of ids of the groups for this user
    #[serde(
        rename = "group_ids",
        default,
        with = "::serde_with::rust::double_option",
        skip_serializing_if = "Option::is_none"
    )]
    pub group_ids: Option<Option<Vec<String>>>,
    /// ID string for user's home folder
    #[serde(
        rename = "home_folder_id",
        default,
        with = "::serde_with::rust::double_option",
        skip_serializing_if = "Option::is_none"
    )]
    pub home_folder_id: Option<Option<String>>,
    /// Unique Id
    #[serde(rename = "id", skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    /// Account has been disabled
    #[serde(rename = "is_disabled", skip_serializing_if = "Option::is_none")]
    pub is_disabled: Option<bool>,
    /// Last name
    #[serde(
        rename = "last_name",
        default,
        with = "::serde_with::rust::double_option",
        skip_serializing_if = "Option::is_none"
    )]
    pub last_name: Option<Option<String>>,
    /// User's preferred locale. User locale takes precedence over Looker's system-wide default locale. Locale determines language of display strings and date and numeric formatting in API responses. Locale string must be a 2 letter language code or a combination of language code and region code: 'en' or 'en-US', for example.
    #[serde(
        rename = "locale",
        default,
        with = "::serde_with::rust::double_option",
        skip_serializing_if = "Option::is_none"
    )]
    pub locale: Option<Option<String>>,
    /// Array of strings representing the Looker versions that this user has used (this only goes back as far as '3.54.0')
    #[serde(
        rename = "looker_versions",
        default,
        with = "::serde_with::rust::double_option",
        skip_serializing_if = "Option::is_none"
    )]
    pub looker_versions: Option<Option<Vec<String>>>,
    /// User's dev workspace has been checked for presence of applicable production projects
    #[serde(
        rename = "models_dir_validated",
        default,
        with = "::serde_with::rust::double_option",
        skip_serializing_if = "Option::is_none"
    )]
    pub models_dir_validated: Option<Option<bool>>,
    /// ID of user's personal folder
    #[serde(
        rename = "personal_folder_id",
        default,
        with = "::serde_with::rust::double_option",
        skip_serializing_if = "Option::is_none"
    )]
    pub personal_folder_id: Option<Option<String>>,
    /// User is identified as an employee of Looker
    #[serde(
        rename = "presumed_looker_employee",
        skip_serializing_if = "Option::is_none"
    )]
    pub presumed_looker_employee: Option<bool>,
    /// Array of ids of the roles for this user
    #[serde(
        rename = "role_ids",
        default,
        with = "::serde_with::rust::double_option",
        skip_serializing_if = "Option::is_none"
    )]
    pub role_ids: Option<Option<Vec<String>>>,
    /// Active sessions
    #[serde(
        rename = "sessions",
        default,
        with = "::serde_with::rust::double_option",
        skip_serializing_if = "Option::is_none"
    )]
    pub sessions: Option<Option<Vec<crate::models::Session>>>,
    /// Per user dictionary of undocumented state information owned by the Looker UI.
    #[serde(
        rename = "ui_state",
        default,
        with = "::serde_with::rust::double_option",
        skip_serializing_if = "Option::is_none"
    )]
    pub ui_state: Option<Option<::std::collections::HashMap<String, String>>>,
    /// User is identified as an employee of Looker who has been verified via Looker corporate authentication
    #[serde(
        rename = "verified_looker_employee",
        skip_serializing_if = "Option::is_none"
    )]
    pub verified_looker_employee: Option<bool>,
    /// User's roles are managed by an external directory like SAML or LDAP and can not be changed directly.
    #[serde(
        rename = "roles_externally_managed",
        skip_serializing_if = "Option::is_none"
    )]
    pub roles_externally_managed: Option<bool>,
    /// User can be directly assigned a role.
    #[serde(rename = "allow_direct_roles", skip_serializing_if = "Option::is_none")]
    pub allow_direct_roles: Option<bool>,
    /// User can be a direct member of a normal Looker group.
    #[serde(
        rename = "allow_normal_group_membership",
        skip_serializing_if = "Option::is_none"
    )]
    pub allow_normal_group_membership: Option<bool>,
    /// User can inherit roles from a normal Looker group.
    #[serde(
        rename = "allow_roles_from_normal_groups",
        skip_serializing_if = "Option::is_none"
    )]
    pub allow_roles_from_normal_groups: Option<bool>,
    /// (Embed only) ID of user's group folder based on the external_group_id optionally specified during embed user login
    #[serde(
        rename = "embed_group_folder_id",
        default,
        with = "::serde_with::rust::double_option",
        skip_serializing_if = "Option::is_none"
    )]
    pub embed_group_folder_id: Option<Option<String>>,
    /// User is an IAM Admin - only available in Looker (Google Cloud core)
    #[serde(rename = "is_iam_admin", skip_serializing_if = "Option::is_none")]
    pub is_iam_admin: Option<bool>,
    /// Link to get this item
    #[serde(
        rename = "url",
        default,
        with = "::serde_with::rust::double_option",
        skip_serializing_if = "Option::is_none"
    )]
    pub url: Option<Option<String>>,
}

impl User {
    pub fn new() -> User {
        User {
            can: None,
            avatar_url: None,
            avatar_url_without_sizing: None,
            credentials_api3: None,
            credentials_email: None,
            credentials_embed: None,
            credentials_google: None,
            credentials_ldap: None,
            credentials_looker_openid: None,
            credentials_oidc: None,
            credentials_saml: None,
            credentials_totp: None,
            display_name: None,
            email: None,
            embed_group_space_id: None,
            first_name: None,
            group_ids: None,
            home_folder_id: None,
            id: None,
            is_disabled: None,
            last_name: None,
            locale: None,
            looker_versions: None,
            models_dir_validated: None,
            personal_folder_id: None,
            presumed_looker_employee: None,
            role_ids: None,
            sessions: None,
            ui_state: None,
            verified_looker_employee: None,
            roles_externally_managed: None,
            allow_direct_roles: None,
            allow_normal_group_membership: None,
            allow_roles_from_normal_groups: None,
            embed_group_folder_id: None,
            is_iam_admin: None,
            url: None,
        }
    }
}
