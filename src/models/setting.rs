/*
 * Looker API 4.0 Reference
 *
 *  API 4.0 is the current release of the Looker API. API 3.1 is deprecated.  ### Authorization  The classic method of API authorization uses Looker **API** credentials for authorization and access control. Looker admins can create API credentials on Looker's **Admin/Users** page.  API 4.0 adds additional ways to authenticate API requests, including OAuth and CORS requests.  For details, see [Looker API Authorization](https://cloud.google.com/looker/docs/r/api/authorization).   ### API Explorer  The API Explorer is a Looker-provided utility with many new and unique features for learning and using the Looker API and SDKs.  For details, see the [API Explorer documentation](https://cloud.google.com/looker/docs/r/api/explorer).   ### Looker Language SDKs  The Looker API is a RESTful system that should be usable by any programming language capable of making HTTPS requests. SDKs for a variety of programming languages are also provided to streamline using the API. Looker has an OpenSource [sdk-codegen project](https://github.com/looker-open-source/sdk-codegen) that provides several language SDKs. Language SDKs generated by `sdk-codegen` have an Authentication manager that can automatically authenticate API requests when needed.  For details on available Looker SDKs, see [Looker API Client SDKs](https://cloud.google.com/looker/docs/r/api/client_sdks).   ### API Versioning  Future releases of Looker expand the latest API version release-by-release to securely expose more and more of the core power of the Looker platform to API client applications. API endpoints marked as \"beta\" may receive breaking changes without warning (but we will try to avoid doing that). Stable (non-beta) API endpoints should not receive breaking changes in future releases.  For details, see [Looker API Versioning](https://cloud.google.com/looker/docs/r/api/versioning).   ### In This Release  API 4.0 version was introduced to make adjustments to API functions, parameters, and response types to fix bugs and inconsistencies. These changes fall outside the bounds of non-breaking additive changes we can make to the previous API 3.1.  One benefit of these type adjustments in API 4.0 is dramatically better support for strongly typed languages like TypeScript, Kotlin, Swift, Go, C#, and more.  See the [API 4.0 GA announcement](https://developers.looker.com/api/advanced-usage/version-4-ga) for more information about API 4.0.  The API Explorer can be used to [interactively compare](https://cloud.google.com/looker/docs/r/api/explorer#comparing_api_versions) the differences between API 3.1 and 4.0.   ### API and SDK Support Policies  Looker API versions and language SDKs have varying support levels. Please read the API and SDK [support policies](https://cloud.google.com/looker/docs/r/api/support-policy) for more information.
 *
 * The version of the OpenAPI document: 4.0.23.20
 *
 * Generated by: https://openapi-generator.tech
 */

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Setting {
    /// Toggle extension framework on or off
    #[serde(
        rename = "extension_framework_enabled",
        skip_serializing_if = "Option::is_none"
    )]
    pub extension_framework_enabled: Option<bool>,
    /// (DEPRECATED) Toggle extension load url on or off. Do not use. This is temporary setting that will eventually become a noop and subsequently deleted.
    #[serde(
        rename = "extension_load_url_enabled",
        skip_serializing_if = "Option::is_none"
    )]
    pub extension_load_url_enabled: Option<bool>,
    /// (DEPRECATED) Toggle marketplace auto install on or off. Deprecated - do not use. Auto install can now be enabled via marketplace automation settings
    #[serde(
        rename = "marketplace_auto_install_enabled",
        skip_serializing_if = "Option::is_none"
    )]
    pub marketplace_auto_install_enabled: Option<bool>,
    #[serde(
        rename = "marketplace_automation",
        skip_serializing_if = "Option::is_none"
    )]
    pub marketplace_automation: Option<Box<crate::models::MarketplaceAutomation>>,
    /// Toggle marketplace on or off
    #[serde(
        rename = "marketplace_enabled",
        skip_serializing_if = "Option::is_none"
    )]
    pub marketplace_enabled: Option<bool>,
    /// Location of Looker marketplace CDN
    #[serde(rename = "marketplace_site", skip_serializing_if = "Option::is_none")]
    pub marketplace_site: Option<String>,
    /// Accept marketplace terms by setting this value to true, or get the current status. Marketplace terms CANNOT be declined once accepted. Accepting marketplace terms automatically enables the marketplace. The marketplace can still be disabled after it has been enabled.
    #[serde(
        rename = "marketplace_terms_accepted",
        skip_serializing_if = "Option::is_none"
    )]
    pub marketplace_terms_accepted: Option<bool>,
    #[serde(
        rename = "privatelabel_configuration",
        skip_serializing_if = "Option::is_none"
    )]
    pub privatelabel_configuration: Option<Box<crate::models::PrivatelabelConfiguration>>,
    #[serde(
        rename = "custom_welcome_email",
        skip_serializing_if = "Option::is_none"
    )]
    pub custom_welcome_email: Option<Box<crate::models::CustomWelcomeEmail>>,
    /// Toggle onboarding on or off
    #[serde(rename = "onboarding_enabled", skip_serializing_if = "Option::is_none")]
    pub onboarding_enabled: Option<bool>,
    /// Change instance-wide default timezone
    #[serde(rename = "timezone", skip_serializing_if = "Option::is_none")]
    pub timezone: Option<String>,
    /// Toggle user-specific timezones on or off
    #[serde(
        rename = "allow_user_timezones",
        skip_serializing_if = "Option::is_none"
    )]
    pub allow_user_timezones: Option<bool>,
    /// Toggle default future connectors on or off
    #[serde(
        rename = "data_connector_default_enabled",
        skip_serializing_if = "Option::is_none"
    )]
    pub data_connector_default_enabled: Option<bool>,
    /// Change the base portion of your Looker instance URL setting
    #[serde(rename = "host_url", skip_serializing_if = "Option::is_none")]
    pub host_url: Option<String>,
    /// (Write-Only) If warnings are preventing a host URL change, this parameter allows for overriding warnings to force update the setting. Does not directly change any Looker settings.
    #[serde(rename = "override_warnings", skip_serializing_if = "Option::is_none")]
    pub override_warnings: Option<bool>,
    /// An array of Email Domain Allowlist of type string for Scheduled Content
    #[serde(
        rename = "email_domain_allowlist",
        skip_serializing_if = "Option::is_none"
    )]
    pub email_domain_allowlist: Option<Vec<String>>,
    /// (DEPRECATED) Use embed_config.embed_cookieless_v2 instead. If embed_config.embed_cookieless_v2 is specified, it overrides this value.
    #[serde(
        rename = "embed_cookieless_v2",
        skip_serializing_if = "Option::is_none"
    )]
    pub embed_cookieless_v2: Option<bool>,
    /// True if embedding is enabled https://cloud.google.com/looker/docs/r/looker-core-feature-embed, false otherwise
    #[serde(rename = "embed_enabled", skip_serializing_if = "Option::is_none")]
    pub embed_enabled: Option<bool>,
    #[serde(rename = "embed_config", skip_serializing_if = "Option::is_none")]
    pub embed_config: Option<Box<crate::models::EmbedConfig>>,
}

impl Setting {
    pub fn new() -> Setting {
        Setting {
            extension_framework_enabled: None,
            extension_load_url_enabled: None,
            marketplace_auto_install_enabled: None,
            marketplace_automation: None,
            marketplace_enabled: None,
            marketplace_site: None,
            marketplace_terms_accepted: None,
            privatelabel_configuration: None,
            custom_welcome_email: None,
            onboarding_enabled: None,
            timezone: None,
            allow_user_timezones: None,
            data_connector_default_enabled: None,
            host_url: None,
            override_warnings: None,
            email_domain_allowlist: None,
            embed_cookieless_v2: None,
            embed_enabled: None,
            embed_config: None,
        }
    }
}
