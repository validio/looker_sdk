/*
 * Looker API 4.0 Reference
 *
 *  API 4.0 is the current release of the Looker API. API 3.1 is deprecated.  ### Authorization  The classic method of API authorization uses Looker **API** credentials for authorization and access control. Looker admins can create API credentials on Looker's **Admin/Users** page.  API 4.0 adds additional ways to authenticate API requests, including OAuth and CORS requests.  For details, see [Looker API Authorization](https://cloud.google.com/looker/docs/r/api/authorization).   ### API Explorer  The API Explorer is a Looker-provided utility with many new and unique features for learning and using the Looker API and SDKs.  For details, see the [API Explorer documentation](https://cloud.google.com/looker/docs/r/api/explorer).   ### Looker Language SDKs  The Looker API is a RESTful system that should be usable by any programming language capable of making HTTPS requests. SDKs for a variety of programming languages are also provided to streamline using the API. Looker has an OpenSource [sdk-codegen project](https://github.com/looker-open-source/sdk-codegen) that provides several language SDKs. Language SDKs generated by `sdk-codegen` have an Authentication manager that can automatically authenticate API requests when needed.  For details on available Looker SDKs, see [Looker API Client SDKs](https://cloud.google.com/looker/docs/r/api/client_sdks).   ### API Versioning  Future releases of Looker expand the latest API version release-by-release to securely expose more and more of the core power of the Looker platform to API client applications. API endpoints marked as \"beta\" may receive breaking changes without warning (but we will try to avoid doing that). Stable (non-beta) API endpoints should not receive breaking changes in future releases.  For details, see [Looker API Versioning](https://cloud.google.com/looker/docs/r/api/versioning).   ### In This Release  API 4.0 version was introduced to make adjustments to API functions, parameters, and response types to fix bugs and inconsistencies. These changes fall outside the bounds of non-breaking additive changes we can make to the previous API 3.1.  One benefit of these type adjustments in API 4.0 is dramatically better support for strongly typed languages like TypeScript, Kotlin, Swift, Go, C#, and more.  See the [API 4.0 GA announcement](https://developers.looker.com/api/advanced-usage/version-4-ga) for more information about API 4.0.  The API Explorer can be used to [interactively compare](https://cloud.google.com/looker/docs/r/api/explorer#comparing_api_versions) the differences between API 3.1 and 4.0.   ### API and SDK Support Policies  Looker API versions and language SDKs have varying support levels. Please read the API and SDK [support policies](https://cloud.google.com/looker/docs/r/api/support-policy) for more information.
 *
 * The version of the OpenAPI document: 4.0.23.20
 *
 * Generated by: https://openapi-generator.tech
 */

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct EmbedConfig {
    /// List of domains to allow for embedding
    #[serde(
        rename = "domain_allowlist",
        default,
        with = "::serde_with::rust::double_option",
        skip_serializing_if = "Option::is_none"
    )]
    pub domain_allowlist: Option<Option<Vec<String>>>,
    /// List of base urls to allow for alert/schedule
    #[serde(
        rename = "alert_url_allowlist",
        default,
        with = "::serde_with::rust::double_option",
        skip_serializing_if = "Option::is_none"
    )]
    pub alert_url_allowlist: Option<Option<Vec<String>>>,
    /// Owner of who defines the alert/schedule params on the base url
    #[serde(
        rename = "alert_url_param_owner",
        default,
        with = "::serde_with::rust::double_option",
        skip_serializing_if = "Option::is_none"
    )]
    pub alert_url_param_owner: Option<Option<String>>,
    /// Label for the alert/schedule url
    #[serde(
        rename = "alert_url_label",
        default,
        with = "::serde_with::rust::double_option",
        skip_serializing_if = "Option::is_none"
    )]
    pub alert_url_label: Option<Option<String>>,
    /// Is SSO embedding enabled for this Looker
    #[serde(rename = "sso_auth_enabled", skip_serializing_if = "Option::is_none")]
    pub sso_auth_enabled: Option<bool>,
    /// Is Cookieless embedding enabled for this Looker
    #[serde(
        rename = "embed_cookieless_v2",
        skip_serializing_if = "Option::is_none"
    )]
    pub embed_cookieless_v2: Option<bool>,
    /// Is embed content navigation enabled for this looker
    #[serde(
        rename = "embed_content_navigation",
        skip_serializing_if = "Option::is_none"
    )]
    pub embed_content_navigation: Option<bool>,
    /// Is embed content management enabled for this Looker
    #[serde(
        rename = "embed_content_management",
        skip_serializing_if = "Option::is_none"
    )]
    pub embed_content_management: Option<bool>,
    /// When true, prohibits the use of Looker login pages in non-Looker iframes. When false, Looker login pages may be used in non-Looker hosted iframes.
    #[serde(
        rename = "strict_sameorigin_for_login",
        skip_serializing_if = "Option::is_none"
    )]
    pub strict_sameorigin_for_login: Option<bool>,
    /// When true, filters are enabled on embedded Looks
    #[serde(rename = "look_filters", skip_serializing_if = "Option::is_none")]
    pub look_filters: Option<bool>,
    /// When true, removes navigation to Looks from embedded dashboards and explores.
    #[serde(
        rename = "hide_look_navigation",
        skip_serializing_if = "Option::is_none"
    )]
    pub hide_look_navigation: Option<bool>,
}

impl EmbedConfig {
    pub fn new() -> EmbedConfig {
        EmbedConfig {
            domain_allowlist: None,
            alert_url_allowlist: None,
            alert_url_param_owner: None,
            alert_url_label: None,
            sso_auth_enabled: None,
            embed_cookieless_v2: None,
            embed_content_navigation: None,
            embed_content_management: None,
            strict_sameorigin_for_login: None,
            look_filters: None,
            hide_look_navigation: None,
        }
    }
}
